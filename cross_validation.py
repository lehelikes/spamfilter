# Balazs Istvan-Lehel, 521/1
# MestInt
# Spam_Cross_Validation

import math

# Spam words we ignore
spam_words = set()

# Email related
email_names = []
email_names_parts = []

# All words we detected to exist
dictionary = set()


def read_spam_words():
    global spam_words

    # Read the spam words
    with open("stopwords.txt") as fin:
        for row in fin:
            spam_words.add(row.strip())

    with open("stopwords2.txt") as fin:
        for row in fin:
            spam_words.add(row.strip())


def is_word(word_or_punctuation):
    symbols = ['.', ',', '/', '?', '>', '<', '\'', '"', ';', ':', '\\', '|', '[', '{', '}', ']', '=', '+', '-', '_',
               ')', '(', '*', '&', '^', '%', '$', '#', '@', '!', '~', '`']
    if word_or_punctuation not in symbols:
        return True
    return False


def read_email_names():
    with open("cross.txt") as fin:
        for email_name in fin:
            email_name = email_name.strip()
            email_names.append(email_name)


def divide_into_parts(k):
    global email_names
    global email_names_parts

    piece = len(email_names) / k
    for i in range(k):
        email_names_parts.append([])
        for j in range(int(piece)):
            email_names_parts[i].append(email_names[j + (i * int(piece))])


def learn_stage(train_data):
    trained_spam_words = dict()
    trained_ham_words = dict()

    for entry in train_data:
        email = open("emails/" + entry, encoding="latin-1")
        for email_row in email:
            email_row_words = email_row.split()
            for word in email_row_words:
                word = word.strip()
                word = word.lower()
                # Tokenize
                if word not in spam_words and is_word(word):
                    dictionary.add(word)
                    if "BG.spam" in entry:
                        if word in trained_spam_words:
                            trained_spam_words[word] += 1
                        else:
                            trained_spam_words[word] = 1
                    elif "lokay.ham" in entry:
                        if word in trained_ham_words:
                            trained_ham_words[word] += 1
                        else:
                            trained_ham_words[word] = 1
        email.close()

    return trained_spam_words, trained_ham_words


def get_probability_of_c(train_data, k):
    spam_email_count = 0
    ham_email_count = 0

    for entry in train_data:
        if "BG.spam" in entry:
            spam_email_count += 1
        elif "lokay.ham" in entry:
            ham_email_count += 1

    piece = len(email_names) / k
    return spam_email_count / (len(email_names) - piece), ham_email_count / (len(email_names) - piece)


def set_prob_of_words(trained_spam_words, trained_ham_words, alpha):
    global dictionary

    word_prob_spam = dict()
    word_prob_ham = dict()

    for dictionary_word in dictionary:
        # Check if it has occurred in spam emails
        if dictionary_word in trained_spam_words:
            word_prob_spam[dictionary_word] = (trained_spam_words[dictionary_word] + alpha) / (
                    sum(trained_spam_words.values()) + alpha * len(dictionary))
        else:
            word_prob_spam[dictionary_word] = alpha / (sum(trained_spam_words.values()) + alpha * len(dictionary))

        # Check if it has occurred in ham emails
        if dictionary_word in trained_ham_words:
            word_prob_ham[dictionary_word] = (trained_ham_words[dictionary_word] + alpha) / (
                    sum(trained_ham_words.values()) + alpha * len(dictionary))
        else:
            word_prob_ham[dictionary_word] = alpha / (sum(trained_ham_words.values()) + alpha * len(dictionary))

    return word_prob_spam, word_prob_ham


def test_stage(test_data, prob_of_spam, prob_of_ham, prob_word_spam, prob_word_ham, alpha, trained_spam_words, trained_ham_words):
    labeled_test_email = dict()

    for entry in test_data:
        entry = entry.strip()
        # Open test email
        email = open("emails/" + entry, encoding="latin-1")
        test_words = dict()

        # Final result variable
        result = math.log(prob_of_spam) - math.log(prob_of_ham)
        summa = 0

        for row_email in email:
            row_email_words = row_email.split()
            for word in row_email_words:
                word = word.strip()
                word = word.lower()
                # Tokenize
                if word not in spam_words and is_word(word):
                    if word in test_words:
                        test_words[word] += 1
                    else:
                        test_words[word] = 1
        email.close()

        for test_word in test_words:
            if test_word in prob_word_spam:
                local_spam = prob_word_spam[test_word]
            else:
                local_spam = alpha / (sum(trained_spam_words.values()) + alpha * len(dictionary))

            if test_word in prob_word_ham:
                local_ham = prob_word_ham[test_word]
            else:
                local_ham = alpha / (sum(trained_ham_words.values()) + alpha * len(dictionary))

            summa += (test_words[test_word] * (math.log(local_spam) - math.log(local_ham)))

        result += summa
        if result > 1:
            labeled_test_email[entry] = "spam"
        else:
            labeled_test_email[entry] = "ham"

    label_good = 0
    label_all = len(labeled_test_email)

    for key, value in labeled_test_email.items():
        if ("BG.spam" in key and value == "spam") or ("lokay.ham" in key and value == "ham"):
            label_good += 1

    return 1 - (label_good / label_all)


def cross_validate(k, alpha):
    global email_names_parts

    test_error = 0

    for i in range(k):
        print("{0}. Started.".format(i))
        # i-edik mindig a teszt halmaz
        test_emails = email_names_parts[i]
        train_emails = []
        for j in range(k):
            if j != i:
                train_emails += email_names_parts[j]

        trained_spam_words, trained_ham_words = learn_stage(train_emails)
        print("{0}. Learn stage passed.".format(i))
        probability_of_spam, probability_of_ham = get_probability_of_c(train_emails, k)
        print("{0}. Got probabilities of spam & ham.".format(i))
        word_prob_spam, word_prob_ham = set_prob_of_words(trained_spam_words, trained_ham_words, alpha)
        print("{0}. Prob. of words set.".format(i))
        local_error = test_stage(test_emails, probability_of_spam, probability_of_ham, word_prob_spam,
                                 word_prob_ham, alpha, trained_spam_words, trained_ham_words)
        print("{0}. Test stage passed.".format(i))
        test_error += local_error

    return test_error / k


def main():
    read_spam_words()
    read_email_names()

    k = 5
    divide_into_parts(k)

    # emulated do:while loop
    alpha_values = []
    error_values = []

    alpha = 1
    alpha_values.append(alpha)
    print("Started algorithm on alpha value: {0}".format(alpha))
    error_avg = cross_validate(k, alpha)
    error_values.append(error_avg)
    print("Average error value on {0} alpha value is: {1}".format(alpha, error_avg))
    while True:
        alpha_now = alpha / 10
        alpha_values.append(alpha)
        print("Started algorithm on alpha value: {0}".format(alpha_now))
        error_avg_now = cross_validate(k, alpha_now)
        error_values.append(error_avg_now)
        print("Average error value on {0} alpha value is: {1}".format(alpha_now, error_avg_now))

        if abs(error_avg - error_avg_now) < 0.001:
            break

        alpha = alpha_now
        error_avg = error_avg_now

    print("Best alpha value is: {0} with avg. error of: {1}".format(alpha_values[error_values.index(min(error_values))], min(error_values)))

if __name__ == '__main__':
    main()
