# Balazs Istvan-Lehel, 521/1
# MestInt
# Spam

import math

# Spam words we ignore
spam_words = set()
# All words we detected to exist
dictionary = set()
all_email_count = 0


def read_spam_words():
    global spam_words

    # Read the spam words
    with open("stopwords.txt") as fin:
        for row in fin:
            spam_words.add(row.strip())

    with open("stopwords2.txt") as fin:
        for row in fin:
            spam_words.add(row.strip())


def is_word(word_or_punctuation):
    symbols = ['.', ',', '/', '?', '>', '<', '\'', '"', ';', ':', '\\', '|', '[', '{', '}', ']', '=', '+', '-', '_',
               ')', '(', '*', '&', '^', '%', '$', '#', '@', '!', '~', '`']
    if word_or_punctuation not in symbols:
        return True
    return False


class C:
    def __init__(self):
        self.words = dict()
        self.count = 0
        self.my_probability = 0

    def learn_stage(self, filter_string):
        global all_email_count
        global dictionary

        with open("train.txt") as fin:
            for row in fin:
                row = row.strip()
                if filter_string in row:
                    email = open("emails/" + row, encoding="latin-1")
                    for email_row in email:
                        email_row_words = email_row.split()
                        for email_word in email_row_words:
                            email_word = email_word.strip()
                            email_word = email_word.lower()
                            # Tokenize
                            if email_word not in spam_words and is_word(email_word):
                                dictionary.add(email_word)
                                if email_word in self.words:
                                    self.words[email_word] += 1
                                else:
                                    self.words[email_word] = 1
                    email.close()
                    self.count += 1
                    all_email_count += 1

    def set_self_probability(self):
        self.my_probability = self.count / all_email_count


word_prob_spam = dict()
word_prob_ham = dict()


def set_prob_of_words_normal(spam, ham):
    global word_prob_spam
    global word_prob_ham
    global dictionary

    for dictionary_word in dictionary:
        # Check if it has occurred in spam emails
        if dictionary_word in spam.words:
            word_prob_spam[dictionary_word] = spam.words[dictionary_word] / sum(spam.words.values())
        else:
            word_prob_spam[dictionary_word] = 0.000001

        # Check if it has occurred in ham emails
        if dictionary_word in ham.words:
            word_prob_ham[dictionary_word] = ham.words[dictionary_word] / sum(ham.words.values())
        else:
            word_prob_ham[dictionary_word] = 0.000001


def set_prob_of_words_simitas(spam, ham, alpha):
    global word_prob_spam
    global word_prob_ham
    global dictionary

    for dictionary_word in dictionary:
        # Check if it has occurred in spam emails
        if dictionary_word in spam.words:
            word_prob_spam[dictionary_word] = (spam.words[dictionary_word] + alpha) / (sum(spam.words.values()) + alpha * len(dictionary))
        else:
            word_prob_spam[dictionary_word] = alpha / (sum(spam.words.values()) + alpha * len(dictionary))

        # Check if it has occurred in ham emails
        if dictionary_word in ham.words:
            word_prob_ham[dictionary_word] = (ham.words[dictionary_word] + alpha) / (sum(ham.words.values()) + alpha * len(dictionary))
        else:
            word_prob_ham[dictionary_word] = alpha / (sum(ham.words.values()) + alpha * len(dictionary))


def test_stage(spam, ham, file, alpha=None):
    global word_prob_spam
    global word_prob_ham
    labeled_test_emails = dict()

    with open(file) as fin:
        for row in fin:
            row = row.strip()
            test_email = open("emails/" + row, encoding="latin-1")
            result = math.log(spam.my_probability) - math.log(ham.my_probability)
            summa = 0
            test_words = dict()

            for row_email in test_email:
                row_email_words = row_email.split()
                for row_email_word in row_email_words:
                    row_email_word = row_email_word.strip()
                    row_email_word = row_email_word.lower()
                    # Tokenize
                    if row_email_word not in spam_words and is_word(row_email_word):
                        if row_email_word in test_words:
                            test_words[row_email_word] += 1
                        else:
                            test_words[row_email_word] = 1
            test_email.close()

            for test_word in test_words:
                if test_word in word_prob_spam:
                    prob_spam = word_prob_spam[test_word]
                else:
                    if alpha:
                        prob_spam = alpha / (sum(spam.words.values()) + alpha * len(dictionary))
                    else:
                        prob_spam = 0.00001

                if test_word in word_prob_ham:
                    prob_ham = word_prob_ham[test_word]
                else:
                    if alpha:
                        prob_ham = alpha / (sum(ham.words.values()) + alpha * len(dictionary))
                    else:
                        prob_ham = 0.00001
                summa += (test_words[test_word] * (math.log(prob_spam) - math.log(prob_ham)))

            result += summa
            if result > 1:
                labeled_test_emails[row] = "spam"
            else:
                labeled_test_emails[row] = 'ham'

    return labeled_test_emails


def main():
    read_spam_words()

    Spam = C()
    Spam.learn_stage("BG.spam")

    Ham = C()
    Ham.learn_stage("lokay.ham")

    Spam.set_self_probability()
    Ham.set_self_probability()

    print("SPAM PROBABILITY: {0}".format(Spam.my_probability))
    print("HAM PROBABILITY: {0}".format(Ham.my_probability))

    # Normal - ha azt akarom hogy simitas nelkul
    # set_prob_of_words_normal(Spam, Ham)
    #
    # # Tanulasi hiba
    # result_labels_train = test_stage(Spam, Ham, "train.txt")
    # label_good = 0
    # label_all = len(result_labels_train)
    #
    # for key, value in result_labels_train.items():
    #     if ("BG.spam" in key and value == "spam") or ("lokay.ham" in key and value == "ham"):
    #         label_good += 1
    #
    # train_error = 1 - (label_good / label_all)
    # print("Train error: {0}".format(train_error))
    #
    # # False positive & negative check
    # label_false_positive = 0
    # label_false_negative = 0
    # label_all = 0
    # for key, value in result_labels_train.items():
    #     if "BG.spam" in key:
    #         if value == "ham":
    #             label_false_negative += 1
    #
    #     if "lokay.ham" in key:
    #         if value == "spam":
    #             label_false_positive += 1
    #     label_all += 1
    #
    # print("False positive error: {0}".format(label_false_positive / label_all))
    # print("False negative error: {0}".format(label_false_negative / label_all))
    #
    # # Teszt hiba
    # result_labels_test = test_stage(Spam, Ham, "test.txt")
    # label_good = 0
    # label_all = len(result_labels_test)
    #
    # for key, value in result_labels_test.items():
    #     if ("BG.spam" in key and value == "spam") or ("lokay.ham" in key and value == "ham"):
    #         label_good += 1
    #
    # test_error = 1 - (label_good / label_all)
    # print("Test error: {0}".format(test_error))
    #
    # # False positive & negative check
    # label_false_positive = 0
    # label_false_negative = 0
    # label_all = 0
    # for key, value in result_labels_test.items():
    #     if "BG.spam" in key:
    #         if value == "ham":
    #             label_false_negative += 1
    #
    #     if "lokay.ham" in key:
    #         if value == "spam":
    #             label_false_positive += 1
    #     label_all += 1
    #
    # print("False positive error: {0}".format(label_false_positive / label_all))
    # print("False negative error: {0}".format(label_false_negative / label_all))
    #
    # print("\n")

    ########################
    ########################
    ########################
    ########################
    ########################
    # Simitassal
    alpha = 1
    while alpha != 0.001:
        set_prob_of_words_simitas(Spam, Ham, alpha)

        # Tanulasi hiba
        result_labels_train = test_stage(Spam, Ham, "train.txt", alpha)
        label_good = 0
        label_all = len(result_labels_train)

        for key, value in result_labels_train.items():
            if ("BG.spam" in key and value == "spam") or ("lokay.ham" in key and value == "ham"):
                label_good += 1

        train_error = 1 - (label_good / label_all)
        print("Train error: {0}".format(train_error))

        # False positive & negative check
        label_false_positive = 0
        label_false_negative = 0
        label_all = 0
        for key, value in result_labels_train.items():
            if "BG.spam" in key:
                if value == "ham":
                    label_false_negative += 1

            if "lokay.ham" in key:
                if value == "spam":
                    label_false_positive += 1
            label_all += 1

        print("False positive error: {0}".format(label_false_positive / label_all))
        print("False negative error: {0}".format(label_false_negative / label_all))

        # Teszt hiba
        result_labels_test = test_stage(Spam, Ham, "test.txt", alpha)
        label_good = 0
        label_all = len(result_labels_test)

        for key, value in result_labels_test.items():
            if ("BG.spam" in key and value == "spam") or ("lokay.ham" in key and value == "ham"):
                label_good += 1

        test_error = 1 - (label_good / label_all)
        print("Test error: {0}".format(test_error))

        # False positive & negative check
        label_false_positive = 0
        label_false_negative = 0
        label_all = 0
        for key, value in result_labels_test.items():
            if "BG.spam" in key:
                if value == "ham":
                    label_false_negative += 1

            if "lokay.ham" in key:
                if value == "spam":
                    label_false_positive += 1
            label_all += 1

        print("False positive error: {0}".format(label_false_positive / label_all))
        print("False negative error: {0}".format(label_false_negative / label_all))

        print("\n")
        alpha /= 10


if __name__ == '__main__':
    main()
